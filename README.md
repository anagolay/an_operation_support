# an_operation_support

an_operation_support is a crate that factors common support functionality to Anagolay Workflow definition and Operation execution. It's composed of the following modules:

- **an_operation_support**: the library crate, intended to be the only package a new Operation must depend onto
- **an_operation_support_macros**: a procedural macro crate, needs to be separate from the rest of the library but it's reexported by **an_operation_support**

## Installation

In the Cargo.toml file of the new Operation, specify a dependency toward **an_operation_support** crate:

```toml
[dependencies]
an_operation_support = { git="https://gitlab.com/anagolay/an_operation_support.git", default-features = false, features = ["anagolay"] }
```

The line above only uses feature _anagolay_ which is intended to be activated to build the library code as dependency for other crates. Note that in this configuration the usage of standard library is avoided.

## Features

- **anagolay** Used to build this crate as dependency for other operations (default)
- **std** Enable usage of standard library, used for macros (default)
- **js** Enable serialization and deserialization functions for javascript (default)
- **async** Provides asynchronous trait for Workflow (default)
- **test** Used to build and execute tests


## Usage

The library allows support for the following repetitive tasks, which occur every time a new Operation or Workflow is created

1) [Manifest generation](https://www.notion.so/kelp/Operation-4e0a156077434877a45a61ad4d25c91e#444e881ece1b4f65a8450535313afa03)
2) [Operation input serialization/deserialization](https://www.notion.so/kelp/Operation-4e0a156077434877a45a61ad4d25c91e#929086f74e2945cd9aedf7dd0453b021)
3) [Command line implementation](https://www.notion.so/kelp/Operation-4e0a156077434877a45a61ad4d25c91e#a49ba1d5af9d445abcfefd645ac4b7a6)
4) Workflow trait (synchronous and asynchronous)

## License

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
#![cfg_attr(not(feature = "std"), no_std)]

#[cfg(not(feature = "std"))]
extern crate alloc;

mod functions;
mod implementations;

use crate::functions::macro_describe;
use proc_macro::TokenStream;

/// This macro attribute is used to annotate the `execute()` function and will provide
/// implementation of the `describe()` function that generates the Operation manifest
///
/// # Examples
/// ```
/// use std::collections::BTreeMap;
/// use an_operation_support_macro::{describe};
///
/// #[describe([
///     groups = [
///       "SYS",
///     ],
///     config = [],
///     features = [
///       "std"
///     ]
/// ])]
/// pub async fn execute(
///     example: &String,
///     _: BTreeMap<String, String>,
/// ) -> Result<String, ()> {
///     Ok(example.to_owned())
/// }
///
/// # assert_eq!("{\"name\":\"an_operation_support_macro\",\"description\":\"Provides shared functionalities to annotate Operation code and generate manifest\",\"inputs\":[\"String\"],\"config\":{},\"groups\":[\"SYS\"],\"output\":\"String\",\"repository\":\"\",\"license\":\"\",\"features\":[\"std\"]}", describe())
/// ```
#[proc_macro_attribute]
pub fn describe(attr: TokenStream, input: TokenStream) -> TokenStream {
    macro_describe(attr, input)
}

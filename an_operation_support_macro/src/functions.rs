extern crate proc_macro;

#[cfg(not(feature = "std"))]
use alloc::{
    collections::btree_map::BTreeMap,
    format,
    string::{String, ToString},
    vec::Vec,
};
#[cfg(feature = "std")]
use std::collections::BTreeMap;

use crate::implementations::{impl_describe_function, impl_describe_function_for_flowcontrol};
use proc_macro::TokenStream;
use quote::ToTokens;
use syn::{
    self, AngleBracketedGenericArguments, Expr, ExprArray, ExprAssign, ExprLit, ExprReference,
    ExprType, ItemFn, LitStr, Path,
};

/// Function executing proc macro attribute `#[describe]`
pub(crate) fn macro_describe(attr: TokenStream, input: TokenStream) -> TokenStream {
    let item: syn::Item = syn::parse(input).unwrap();
    match item {
        syn::Item::Fn(ref execute_fn) => {
            assert!(
                execute_fn.sig.asyncness.is_some(),
                "Expected execute() function to be async"
            );
            let inputs = parse_signature_input(execute_fn);
            let output = parse_signature_output(execute_fn);
            let (groups, config, features) = parse_attribute(attr);
            impl_describe_function(inputs, output, groups, config, features, execute_fn)
        }
        syn::Item::Macro(ref execute_macro) => {
            let (groups, config, features) = parse_attribute(attr);
            impl_describe_function_for_flowcontrol(groups, config, features, execute_macro)
        }
        _ => {
            assert!(
                false,
                "#[describe] may only be applied to execute() function"
            );
            unreachable!();
        }
    }
}

fn parse_attribute(attr: TokenStream) -> (Vec<String>, BTreeMap<String, Vec<String>>, Vec<String>) {
    let attr_syntree: ExprArray =
        syn::parse(attr).expect("Expected expression array in describe(...)");
    let mut groups = Vec::new();
    let mut config: BTreeMap<String, Vec<String>> = BTreeMap::new();
    let mut features = Vec::new();
    attr_syntree.elems.iter().enumerate().for_each(|(index, expr)| {
        let assign: ExprAssign = syn::parse2(expr.to_token_stream())
            .expect("Expected assignment in describe([...])");
        let left: Path = syn::parse2(assign.left.to_token_stream())
            .expect(&format!("Expected key of assignment at element {} in describe([...])", index));
        let key = &left.segments.last().unwrap().ident.to_string();
        match key.as_bytes() {
            b"groups" => {
                let right: ExprArray = syn::parse2(assign.right.to_token_stream())
                    .expect(&format!("Expected expression array as value of assignment in describe([{}=...])", key));
                right.elems.iter().enumerate().for_each(|(index, expr)| {
                    let group = parse_literal(&expr, &format!("as value of array of assignment at index {} in describe([{}=[...]])", index, key));
                    groups.push(group.value());
                });
            },
            b"config" => {
                let right: ExprArray = syn::parse2(assign.right.to_token_stream())
                    .expect(&format!("Expected expression array as value of assignment in describe([{}=...])", key));
                right.elems.iter().enumerate().for_each(|(cfg_index, expr)| {
                    let assign: ExprAssign = syn::parse2(expr.to_token_stream())
                        .expect(&format!("Expected assignment as value of array in assignement at index {} of describe([{}=[...]])", index, key));
                    let left: Path = syn::parse2(assign.left.to_token_stream())
                        .expect(&format!("Expected key of assignment at element {} in describe([{}=[...]])", cfg_index, key));
                    let cfg_key = &left.segments.last().unwrap().ident.to_string();
                    let right: ExprArray = syn::parse2(assign.right.to_token_stream())
                        .expect(&format!("Expected expression array as value of assignment in describe([{}=[{}=...])", key, cfg_key));
                    let mut cfg_allowed = Vec::new();
                    right.elems.iter().enumerate().for_each(|(cfg_index, expr)| {
                        let allowed = parse_literal(&expr, &format!("Expected string as value of array of assignment at index {} in describe([{}=[{}=[...]])", cfg_index, key, cfg_key));
                        cfg_allowed.push(allowed.value());
                    });
                    config.insert(cfg_key.clone(), cfg_allowed);
                });
            },
            b"features" => {
                let right: ExprArray = syn::parse2(assign.right.to_token_stream())
                    .expect(&format!("Expected expression array as value of assignment in describe([{}=...])", key));
                right.elems.iter().enumerate().for_each(|(index, expr)| {
                    let feature = parse_literal(&expr, &format!("Expected string as value of array of assignment at index {} in describe([{}=[...]])", index, key));
                    features.push(feature.value());
                });
            },
            _ => assert!(
                false,
                "Expected one of \"groups\", \"config\", \"features\" as key of assignment at index {} of describe([...]), found \"{}\"", index, key
            )
        }
    });
    (groups, config, features)
}

fn parse_signature_output(execute_fn: &ItemFn) -> String {
    match execute_fn.sig.output {
        syn::ReturnType::Type(ref _arrow, ref ret_type) => {
            let output_type: Path = syn::parse2(ret_type.to_token_stream())
                .expect("Expected signature to have a return type");
            let output_type_segment = output_type.segments.last().unwrap();
            let result_ok: Path = if output_type_segment.ident == "Result" {
                let result_ok: AngleBracketedGenericArguments =
                    syn::parse2(output_type_segment.arguments.to_token_stream()).unwrap();
                let result_ok: Path =
                    syn::parse2(result_ok.args.iter().next().unwrap().to_token_stream())
                        .expect("Expected signature return type as Result OK to be a path");
                result_ok
            } else {
                assert!(false, "Expected signature return type to be a Result");
                unreachable!()
            };
            result_ok.segments.iter().fold(String::new(), |acc, ps2| {
                if acc.len() == 0 {
                    ps2.ident.to_string()
                } else {
                    acc + "::" + &ps2.ident.to_string()
                }
            })
        }
        _ => {
            assert!(false, "Expected signature return type");
            unreachable!()
        }
    }
}

fn parse_signature_input(execute_fn: &ItemFn) -> Vec<String> {
    let mut inputs = Vec::<String>::new();
    execute_fn.sig.inputs.iter().for_each(|sig_input| {
        let param: ExprType =
            syn::parse2(sig_input.to_token_stream()).expect("Expected signature param");
        match param.expr.as_ref() {
            syn::Expr::Verbatim(ref _verbatim) => {}
            _ => {
                let param_name: Path = syn::parse2(param.expr.to_token_stream())
                    .expect("Expected signature param name");
                let param_name = param_name.segments.last().unwrap().ident.to_string();
                if param_name != "config" {
                    let param_type: ExprReference = syn::parse2(param.ty.to_token_stream())
                        .expect("Expected signature param type");
                    let param_type: Path = syn::parse2(param_type.expr.to_token_stream())
                        .expect("Expected signature param type expression");
                    let param_type = param_type.segments.iter().fold(String::new(), |acc, ps2| {
                        if acc.len() == 0 {
                            ps2.ident.to_string()
                        } else {
                            acc + "::" + &ps2.ident.to_string()
                        }
                    });
                    inputs.push(param_type);
                }
            }
        };
    });
    inputs
}

fn parse_literal(expr: &Expr, expect_msg: &str) -> LitStr {
    let lit: ExprLit = syn::parse(TokenStream::from(expr.to_token_stream()))
        .expect(&format!("Expected literal {}", expect_msg));
    let lit: LitStr = syn::parse(TokenStream::from(lit.to_token_stream()))
        .expect(&format!("Expected string {}", expect_msg));
    lit
}

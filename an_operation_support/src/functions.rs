/// Module containing the shared implementation of an Operation command line interface
#[cfg(feature = "std")]
pub mod cli {
    /// Generalization of a main function of an Operation
    /// Provides implementation of a command line interface with the following commands:
    ///
    /// -h, --help        Print help information
    /// -m, --manifest    Produces the Operation manifest data
    /// -V, --version     Print version information
    ///
    /// # Arguments
    ///  * main_args: a [`MainArgs`] structure injecting the required information
    ///  and behavior from the supported Operation
    ///
    /// # Examples
    /// ```should_panic
    ///
    /// # fn describe() -> String {
    /// #     "".to_string()
    /// # }
    /// pub fn main() {
    ///     let main_args = an_operation_support::MainArgs {
    ///         app_name: env!("CARGO_PKG_NAME"),
    ///         app_version: env!("CARGO_PKG_VERSION"),
    ///         describe: &|| crate::describe()
    ///     };
    ///     an_operation_support::main(&main_args);
    /// }
    /// ```
    pub fn main(main_args: &crate::types::cli::MainArgs) {
        use clap::{Arg, Command};

        let matches = Command::new(main_args.app_name)
            .version(main_args.app_version)
            .arg(
                Arg::new("manifest")
                    .short('m')
                    .long("manifest")
                    .takes_value(false)
                    .help("Produces the Operation manifest data"),
            )
            .arg_required_else_help(true)
            .get_matches();

        if matches.is_present("manifest") {
            let manifest: String = (main_args.describe)();
            println!("{}", manifest);
        }
    }
}

use crate::Bytes;
#[cfg(not(feature = "std"))]
use alloc::{borrow::ToOwned, collections::btree_map::BTreeMap};
use core::any::Any;
use js_sys::{Map, Uint8Array};
use serde::{de::DeserializeOwned, Serialize};
#[cfg(feature = "std")]
use std::collections::BTreeMap;
use wasm_bindgen::{JsCast, JsValue};

/// Serialize an encodable object to a js byte array
///
/// # Arguments
///  * data: an object serializable by bincode
///  * decode: do not encode value to be used as input for next operation, it's a workflow output
///
/// # Return
/// A Result having [`JsValue`] types containing a js byte array as `Ok` and an error message as `Err`
pub fn to_value<I: 'static + Serialize>(data: &I) -> Result<JsValue, JsValue> {
    let data_any = data as &dyn Any;
    match data_any.downcast_ref::<Bytes>() {
        // Serialize bytes directly to a js array. This method is preferable in performance cost
        // than the serialize method if the data is already in the form of bytes
        Some(as_bytes) => {
            let len = as_bytes.len() as u32;
            let input = Uint8Array::new_with_length(len);
            input.copy_from(&as_bytes);
            Ok(input.into())
        }
        None => Ok(serde_wasm_bindgen::to_value(data)?),
    }
}

/// Deserialize the operation input.
/// If the object is a js byte array, bincode will be used because it's faster
/// In other cases, it defaults to serde_wasm_bindgen but this requires a copy of memory and is
/// less performant
///
/// # Arguments
///  * operation_input: a ['JsValue']
///
/// # Return
/// A [`Result`] containing the expected object if deserialization was successful, a
/// [`JsValue`] error message otherwise
pub fn from_value<I: 'static + DeserializeOwned>(operation_input: &JsValue) -> Result<I, JsValue> {
    Ok(serde_wasm_bindgen::from_value(operation_input.to_owned())?)
}

/// Deserialize the operation input as Bytes
///
/// # Arguments
///  * operation_input: a ['JsValue']
///
/// # Return
/// A [`Result`] containing the expected object if deserialization was successful, a
/// [`JsValue`] error message otherwise
pub fn from_bytes(operation_input: &JsValue) -> Result<Bytes, JsValue> {
    let cast: Option<&Uint8Array> = operation_input.dyn_ref();
    match cast {
        Some(array) => Ok(array.to_vec()),
        None => Err(JsValue::from_str(
            "Expected a JsValue that could be casted to UInt8Array",
        )),
    }
}

/// Converts a js Map into a BTreeMap
///
/// # Arguments
///  * js_map: js map
///
/// # Return
/// A [`Result`] containing the expected object if deserialization was successful, a
/// [`JsValue`] error message otherwise
pub fn from_map<K: DeserializeOwned + Ord, V: DeserializeOwned>(
    js_map: &Map,
) -> Result<BTreeMap<K, V>, JsValue> {
    serde_wasm_bindgen::from_value(js_map.into()).map_err(|error| error.into())
}

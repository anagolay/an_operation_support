#[cfg(not(feature = "std"))]
use alloc::vec::Vec;

/// Alias for a collection of bytes
pub type Bytes = Vec<u8>;
/// Alias for the bytes of a Cid
pub type GenericId = Vec<u8>;

mod structure {
    use crate::GenericId;
    use core::option::Option;
    #[cfg(feature = "std")]
    use serde::{Deserialize, Serialize};

    /// Generic structure that contains some data and a cid of the same data.
    /// This is not exported as it's meant to be aliased by specific entities like
    /// Workflow, Operation, etc.
    ///
    /// # Type Arguments
    ///  * T: the type of the data
    #[cfg_attr(feature = "std", derive(Serialize, Deserialize))]
    #[derive(Debug, PartialEq, Clone)]
    pub struct AnagolayStructure<T> {
        /// Content id of the data. It's [Option] because the cid
        /// is not available when creating new entities off-chain
        #[cfg_attr(feature = "std", serde(with = "serde_bytes"))]
        #[cfg_attr(feature = "std", serde(skip_serializing_if = "Option::is_none"))]
        #[cfg_attr(feature = "std", serde(default))]
        pub id: Option<GenericId>,
        /// The data
        pub data: T,
    }
}

/// Module containing the data model of the Workflow manifest
pub mod workflow {
    use crate::types::{structure::AnagolayStructure, GenericId};
    #[cfg(not(feature = "std"))]
    use alloc::{boxed::Box, collections::btree_map::BTreeMap, string::String, vec::Vec};
    #[cfg(feature = "std")]
    use {
        serde::{Deserialize, Serialize},
        std::{collections::BTreeMap, string::String},
    };

    /// Definition of an Operation to execute in a Workflow Segment
    #[cfg_attr(feature = "std", derive(Serialize, Deserialize))]
    #[derive(Debug, PartialEq, Clone)]
    #[cfg_attr(feature = "std", serde(rename_all = "camelCase"))]
    pub struct OperationVersionReference {
        /// The Version id of the Operation to execute
        #[cfg_attr(feature = "std", serde(with = "serde_bytes"))]
        pub version_id: GenericId,
        /// The map representing the Operation configuration to apply upon execution
        pub config: BTreeMap<String, String>,
        /// Input types, useful when they are not known at manifest generation time
        #[cfg_attr(feature = "std", serde(skip_serializing_if = "Option::is_none"))]
        pub inputs: Option<Vec<String>>,
        /// Output type, useful when it's not known at manifest generation time
        #[cfg_attr(feature = "std", serde(skip_serializing_if = "Option::is_none"))]
        pub output: Option<String>,
    }

    /// Contains a sequence of Operations, the eventual configuration of each one
    /// of them, and a reference to the input required to bootstrap the process. In fact, the required
    /// input may come from other Segments of the Workflow or from external input as well (eg:
    /// end-user interaction)
    #[cfg_attr(feature = "std", derive(Serialize, Deserialize))]
    #[derive(Debug, PartialEq, Clone)]
    pub struct SegmentDefinition {
        /// The collection of inputs for this segment, where a number lesser than zero means that the
        /// input must be acquired from the outside world (e.g.: user interaction) rather then from a
        /// precedently executed Workflow Segment (thus, its index)
        pub inputs: Vec<i32>,
        /// The sequence of operations to execute in this Segment
        pub sequence: Vec<OperationVersionReference>,
    }

    /// Data that describes a Workflow
    #[cfg_attr(feature = "std", derive(Serialize, Deserialize))]
    #[derive(Debug, PartialEq, Clone)]
    pub struct WorkflowManifestData {
        /// Human readable Workflow name. min 8, max 128(0.12kb) characters, slugify to use _
        pub name: String,
        /// Identifier of the creator users or system as a reference to his account id on the
        /// blockchain, pgp key or email
        pub creators: Vec<String>,
        /// Description can be markdown but not html. min 8, max 1024(1kb) chars
        pub description: String,
        /// Tells which groups the Workflow belongs to
        pub groups: Vec<String>,
        /// A list of Segment definitions
        pub segments: Vec<SegmentDefinition>,
    }

    /// Manifest of a Workflow
    pub type WorkflowManifest = AnagolayStructure<WorkflowManifestData>;

    #[cfg(not(feature = "std"))]
    pub type RcAny = alloc::rc::Rc<dyn core::any::Any>;

    #[cfg(feature = "std")]
    pub type RcAny = std::rc::Rc<dyn core::any::Any>;

    /// This trait represent the result of execution of a Segment in a Workflow
    #[cfg(not(feature = "js"))]
    pub trait SegmentResult {
        /// # Return
        /// True if Workflow execution is completed, false otherwise
        fn is_done(&self) -> bool;

        /// # Return
        /// Some output of the Workflow if its execution is completed, None otherwise
        /// The output needs to be downcasted to the expected type
        fn get_output(&self) -> Option<RcAny>;

        /// # Return
        /// The last Segment execution time in milliseconds
        fn get_segment_time(&self) -> usize;

        /// # Return
        /// The total Workflow execution time in milliseconds
        fn get_total_time(&self) -> usize;
    }

    /// Workflow trait. All Workflows implement this and it gives a handle on the methods:
    /// * new() - Constructor
    /// * next() - invokes next Segment execution
    #[cfg_attr(feature = "async", async_trait::async_trait(?Send))]
    pub trait Workflow {
        /// # Return
        /// A new instance of this Workflow
        fn new() -> Self;

        /// Asynchronously executes the next Segment in the Workflow
        ///
        /// # Arguments
        /// * operation_inputs - Collection of Any reference-counted inputs to fill in the external
        /// inputs (e.g.: input coming from user) for this segment
        ///
        /// # Return
        /// A Result. The `Ok` is the promise result,
        /// while the `Err`, in case it happens, is thrown as a javascript exception
        #[cfg(all(feature = "async", not(feature = "js")))]
        async fn next(self, operation_inputs: Vec<RcAny>)
            -> Result<Box<dyn SegmentResult>, String>;

        /// Synchronously executes the next Segment in the Workflow
        ///
        /// # Arguments
        /// * operation_inputs - Collection of Any reference-counted inputs to fill in the external
        /// inputs (e.g.: input coming from user) for this segment
        ///
        /// # Return
        /// A Result. The `Ok` is the promise result,
        /// while the `Err`, in case it happens, is thrown as a javascript exception
        #[cfg(all(not(feature = "async"), not(feature = "js")))]
        fn next(self, operation_inputs: Vec<RcAny>) -> Result<Box<dyn SegmentResult>, String>;

        /// Asynchronously executes the next Segment in the Workflow
        ///
        /// # Arguments
        /// * operation_inputs - Collection of `Uint8Array`, serialized inputs to fill in the external
        /// inputs (e.g.: input coming from user) for this segment
        ///
        /// # Return
        /// A Result bound by WASM to a javascript promise. The `Ok` is the promise result,
        /// while the `Err`, in case it happens, is thrown as a javascript exception
        #[cfg(all(feature = "async", feature = "js", target_arch = "wasm32"))]
        async fn next(
            self,
            operation_inputs: Vec<wasm_bindgen::JsValue>,
        ) -> Result<wasm_bindgen::JsValue, wasm_bindgen::JsValue>;

        /// Synchronously executes the next Segment in the Workflow
        ///
        /// # Arguments
        /// * operation_inputs - Collection of `Uint8Array`, serialized inputs to fill in the external
        /// inputs (e.g.: input coming from user) for this segment
        ///
        /// # Return
        /// A Result bound by WASM to a javascript promise. The `Ok` is the promise result,
        /// while the `Err`, in case it happens, is thrown as a javascript exception
        #[cfg(all(not(feature = "async"), feature = "js", target_arch = "wasm32"))]
        fn next(
            self,
            operation_inputs: Vec<wasm_bindgen::JsValue>,
        ) -> Result<wasm_bindgen::JsValue, wasm_bindgen::JsValue>;
    }
}

/// Module containing the entities related to Operation implementation
pub mod operation {
    use crate::types::structure::AnagolayStructure;
    #[cfg(not(feature = "std"))]
    use alloc::{collections::btree_map::BTreeMap, string::String, vec::Vec};
    #[cfg(feature = "std")]
    use {
        serde::{Deserialize, Serialize},
        std::{collections::BTreeMap, string::String},
    };

    /// Data that describes an Operation
    #[cfg_attr(feature = "std", derive(Serialize, Deserialize))]
    #[derive(Debug, PartialEq, Clone)]
    pub struct OperationManifestData {
        /// Operation name. min 8, max 128(0.12kb) characters, slugify to use _
        pub name: String,
        /// Description can be markdown but not html. min 8, max 1024(1kb) chars
        pub description: String,
        /// What operation accepts in the implementation. these are the params of the function with the
        /// types
        pub inputs: Vec<String>,
        /// A map where keys are names of configuration parameters and values are collections of strings
        /// representing allowed values
        pub config: BTreeMap<String, Vec<String>>,
        /// A switch used to generate the Workflow segments  
        pub groups: Vec<String>,
        /// Data type name defining the operation output
        pub output: String,
        /// The fully qualified URL for the repository, this can be any public repo URL. min 8, max
        /// 128(0.12kb) characters
        pub repository: String,
        /// Short name of the license, like "Apache-2.0". min 8, max 128(0.12kb) characters,
        pub license: String,
        /// Indicator of the features of the binary. Typically the following
        /// - `config_<key>` with _key_ coming from the config map allows conditional compilation of the
        ///   feature `config_<key>_<value>` where _value_ is the configuration selected at the moment
        ///   the operation is instantiated
        /// - `std` declares support for nostd as default and possibility to work with std. If this
        ///   feature is missing, the operation is intended to be working **only** in std
        pub features: Vec<String>,
    }

    /// Manifest of an Operation
    pub type OperationManifest = AnagolayStructure<OperationManifestData>;
}

/// Module containing the shared implementation of an Operation command line interface
pub mod cli {
    #[cfg(not(feature = "std"))]
    use alloc::string::String;

    /// Structure used to implement the main function of an Operation.
    pub struct MainArgs<'a> {
        /// The app name to show in the command line help message
        pub app_name: &'a str,
        /// The app version to show in the command line help message
        pub app_version: &'a str,
        /// The describe function used for the --manifest command
        pub describe: &'a dyn core::ops::Fn() -> String,
    }
}
